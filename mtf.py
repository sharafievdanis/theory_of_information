from typing import List, AnyStr
from dataclasses import dataclass
from loguru import logger

from bwt import Coder
from bwt import BWT


@dataclass
class MTF(Coder):
    """
    Преобразование MTF (англ. move-to-front, движение к началу) — алгоритм кодирования, используемый
    для предварительной обработки данных (обычно потока байтов) перед сжатием, разработанный для
    улучшения эффективности последующего кодирования.

    Вообще, современные алгоритмы (например, bzip2[1]) перед алгоритмом MTF используют алгоритм BWT
    """

    string_to_encode: AnyStr
    external_coder = BWT

    def encode(self) -> List[int]:
        string_to_encode: str = self.string_to_encode
        bwt: BWT = self.external_coder(string_to_encode)
        last_column, index = bwt.run()

        dictionary = list(range(256))

        text_in_bytes: bytes = last_column.encode("utf-8")

        compressed_text = list()
        logger.info("STARTING MTF...")
        logger.info(f"INITIAL LIST OF INDEXES - {dictionary}\n")
        for chr in text_in_bytes:
            logger.info(f"CURRENT SYMBOL - {chr}")
            idx = dictionary.index(chr)
            compressed_text.append(idx)

            dictionary.pop(idx)
            dictionary.insert(0, chr)
            logger.info(f"LIST AFTER {chr} - {dictionary}\n")

        logger.info(f"FINISHING MTF...RESULT = {compressed_text}")
        return compressed_text  # Return the encoded text

    def run(self) -> List[int]:
        encoded_message = self.encode()
        return encoded_message


def main():
    string_to_encode = input("Что кодируем: ")

    mtf = MTF(string_to_encode=string_to_encode)
    return mtf.run()


if __name__ == "__main__":
    main()
