from typing import AnyStr, List, Tuple
from base import Coder
from dataclasses import dataclass, field
from loguru import logger


@dataclass
class BWT(Coder):

    string_to_transform: AnyStr
    table_of_shifts: List[str] = field(default_factory=list)

    def fill_table_of_shifts(self) -> List[str]:
        """
        Complexity -  O(n), where n - length of input
        :return:
        """
        symbols = list(self.string_to_transform)

        for i in range(len(symbols)):
            word = self.string_to_transform[-1] + self.string_to_transform[:-1]
            new = "".join(word)
            self.string_to_transform = new
            self.table_of_shifts.append(new)
        return self.table_of_shifts

    def sort_table_alphabetically(self, table: List[str]) -> Tuple[List[str], int]:
        """
        Time complexity of sort operation - O(n*log(n))
        """

        def get_index_of_initial_string(sorted_table: list) -> int:
            return sorted_table.index(self.string_to_transform)

        sorted_table = sorted(table)
        return (sorted_table, get_index_of_initial_string(sorted_table))

    def get_last_column(self, sorted_bwt: List[str]):
        """
        O(n), where n - size of sorted list
        """
        result = []
        for word in sorted_bwt:
            last_symbol = word[-1]
            result.append(last_symbol)
        return "".join(result)

    def run(self):
        self.fill_table_of_shifts()
        sorted_table_of_shifts, index = self.sort_table_alphabetically(
            self.table_of_shifts
        )
        last_column = self.get_last_column(sorted_table_of_shifts)
        logger.info(
            f"SUCCESSFULLY FINISHING BWT; RESULT = {last_column}, INDEX = {index}\n"
        )
        return last_column, index
